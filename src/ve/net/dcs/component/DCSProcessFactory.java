package ve.net.dcs.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

import ve.net.dcs.process.GenerateFileFromFormatPrinterForm;
import ve.net.dcs.process.LVE_GenerateBankFileFromPrintFormat;



public class DCSProcessFactory implements IProcessFactory {

	public DCSProcessFactory() {

	}
	@Override
	public ProcessCall newProcessInstance(String className) {
		if (className.equals(LVE_GenerateBankFileFromPrintFormat.class.getName()))
			return new LVE_GenerateBankFileFromPrintFormat();
		if (className.equals(GenerateFileFromFormatPrinterForm.class.getName()))
			return new GenerateFileFromFormatPrinterForm();
		return null;
	}
}
