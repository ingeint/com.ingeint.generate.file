/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 * Contributor(s): Victor Suárez www.dcs.net.ve								  *
 *****************************************************************************/


package ve.net.dcs.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttachment;
import org.compiere.model.MColumn;
import org.compiere.model.MProcess;
import org.compiere.model.MTable;
import org.compiere.model.Query;
import org.compiere.model.X_AD_ReportView_Col;
import org.compiere.print.MPrintFormat;
import org.compiere.print.MPrintFormatItem;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;


/**
 * Localizacion Venezuela
 * LVE_GenerateTxtBank
 * @author Freddy Heredia  -  freddy.heredia@ingeint.ec  - ingeint.ec.
 * @Contributor Victor Suarez  -  vsuarez@dcs.net.ve  - Double Click Sistemas C.A.
 * 2013
 *
 */
public class LVE_GenerateBankFileFromPrintFormat extends SvrProcess {

	public LVE_GenerateBankFileFromPrintFormat() {
		
		
	}
	/** Record_ID                      */
	private int p_Record_ID=0;
	private String p_FormatType = "";
	private String p_FileName="";
	private int p_AD_Process_ID = 0;
	private int p_C_Bank_ID=0;
	private int p_AD_PrintFormat_ID = 0;
	private int	m_created = 0;
	private MPrintFormat printFormat = null;
	protected void prepare() {
	
		p_Record_ID = getRecord_ID();
		
		ProcessInfoParameter[] para = getParameter();
		
		for (int i = 0; i < para.length; i++ )
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("AD_Process_ID"))
				p_AD_Process_ID = para[i].getParameterAsInt();	
			if (name.equals("FormatType")){
				if (para[i].getParameterAsString()!=null)
					p_FormatType = para[i].getParameterAsString();}
			else if (name.equals("FileName")){
				if (para[i].getParameterAsString()!=null)
					p_FileName = para[i].getParameterAsString();
			}
				
			else if (name.equals("C_Bank_ID"))
				p_C_Bank_ID = para[i].getParameterAsInt();
			else if (name.equals("AD_PrintFormat_ID"))
				p_AD_PrintFormat_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	protected String doIt() throws Exception {
		File file = null;
		
		StringBuilder msglog = new StringBuilder("AD_Process_ID=").append(p_AD_Process_ID).append(" C_BankAccount_ID=").append(p_C_Bank_ID).append(" PayrollProcess_ID=").append(p_Record_ID);
		if (log.isLoggable(Level.INFO)) log.info(msglog.toString());
		
		if (p_AD_Process_ID <= 0 && p_AD_PrintFormat_ID <= 0) {
			throw new IllegalArgumentException(Msg.translate(Env.getCtx(), "Process Required!"));
		}
		
		MProcess formatProcess = null;
		if (p_AD_Process_ID > 0){
			formatProcess = MProcess.get(Env.getCtx(), p_AD_Process_ID);
			file = GenerateFileFromProcess (formatProcess);
		}
		if (p_AD_PrintFormat_ID > 0){
			printFormat = MPrintFormat.get(Env.getCtx(), p_AD_PrintFormat_ID, true);
			file = GenerateFileFromPrintFormat (printFormat);
		}		
		
		if (toAttach(file)){
			log.log(Level.SEVERE, "File Name: " + file.getName());
		}else{
			
		}
		StringBuilder msgreturn = new StringBuilder("@Created@ = ").append(m_created);
		return msgreturn.toString();
			
}
	private File GenerateFileFromProcess (MProcess p_formatProcess) throws FileNotFoundException{
		File p_file = null;
		StringBuffer sql =new StringBuffer();;
		StringBuffer body =new StringBuffer();
		StringBuffer line =new StringBuffer();
		String tableName ="";
		String whereClause="";
		String OrderByClause ="";
		boolean replaceColumn = false;
		Vector<KeyNamePair> vSortNo = new Vector<KeyNamePair>();
		printFormat =(MPrintFormat) p_formatProcess.getAD_PrintFormat();
		List <X_AD_ReportView_Col> reportViewCol = null;
		
		if (p_FileName == null)
			throw new AdempiereException(Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "FileName"));
		else if (p_C_Bank_ID == 0)
			throw new AdempiereException(Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "C_Bank_ID"));
		else if (printFormat == null)
			throw new AdempiereException(Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "PrintFormat"));
		
		p_file=new File(p_FileName);
		Scanner scanner = new Scanner(p_file);
		scanner.useDelimiter("\r\n");
		
		if (printFormat.getAD_ReportView() != null){
			tableName = printFormat.getAD_ReportView().getAD_Table().getTableName();
			if (printFormat.getAD_ReportView().getWhereClause()!=null)
				whereClause = " AND "+printFormat.getAD_ReportView().getWhereClause();
			if ( printFormat.getAD_ReportView().getOrderByClause()!=null)
				OrderByClause = printFormat.getAD_ReportView().getOrderByClause();
			
			reportViewCol = new Query (this.getCtx(),X_AD_ReportView_Col.Table_Name,"AD_ReportView_ID = ?",
					null).setParameters(printFormat.getAD_ReportView().getAD_ReportView_ID()).list();
		}
		else{
			tableName =printFormat.get_TableName();
		}
		
		List<MPrintFormatItem> formatItem = new Query (getCtx(),MPrintFormatItem.Table_Name," AD_PrintFormat_ID = ? AND IsPrinted = ? ",
				null).setParameters(printFormat.get_ID(),"Y").setOrderBy(" SeqNo ").list();
		
		sql.append("SELECT ");
		int items = 0;
		for (MPrintFormatItem formatI : formatItem){
			items++;
			
			if (reportViewCol != null){
				for (X_AD_ReportView_Col rwc : reportViewCol){
					MColumn colum = MColumn.get(getCtx(), rwc.getAD_Column_ID());
					if(formatI.getColumnName().equalsIgnoreCase(colum!=null?colum.getColumnName():"")){
						replaceColumn = true;
						sql.append(rwc.getFunctionColumn());
					}else{
						//
					}
				}
			}
			
			if (!replaceColumn){
				sql.append(formatI.getColumnName());
			}else{
				replaceColumn = false; //Inicializo.
			}
				
			if (formatItem.size()!=items){
				sql.append(", ");
				vSortNo.add(new KeyNamePair(formatI.getSortNo(), formatI.getColumnName()));
				
			}
		}
		
		sql.append(" FROM "+tableName);
		sql.append(" WHERE HR_Process_ID =");
		sql.append(p_Record_ID);
		sql.append(" AND BP_C_Bank_ID = ");
		sql.append(p_C_Bank_ID);
		sql.append(" "+whereClause);
		if (OrderByClause.length()>0){
			sql.append(" ORDER BY "+OrderByClause);
		}
			
		log.log(Level.INFO, "SQL: " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery(); 
			while (rs.next())
			{
				line = new StringBuffer();
				for (int i=1;i<=items;i++){
					line.append(rs.getString(i));
					if (i!=items){
						if (p_FormatType.contains("\\t"))
							line.append(p_FormatType.replace("\\t", "\t"));
						else
							line.append(p_FormatType);						
					}
					
				}
				line.append("\r\n");
				
				body.append(line.toString());
			}
			pstmt.close();
			
		}
		catch ( Exception e )
        {
			log.warning(Msg.translate(Env.getCtx(),e.getMessage()));
			throw new AdempiereException(e);
        }
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		try {
			java.io.FileWriter file = new java.io.FileWriter(p_file);
			java.io.BufferedWriter bw = new java.io.BufferedWriter(file);
			java.io.PrintWriter pw = new java.io.PrintWriter(bw); 
			pw.write(body.toString());
			pw.close();
			bw.close();	
		}catch (IOException ioe) {
			log.warning(Msg.translate(Env.getCtx(),ioe.getMessage()));
			throw new AdempiereException(ioe);
		}
		m_created++;
		return p_file;
	}
	
	private File GenerateFileFromPrintFormat (MPrintFormat printFormat){
		File p_file = null;
		StringBuffer sql =new StringBuffer();
		StringBuffer head = new StringBuffer();
		StringBuffer body =new StringBuffer();
		StringBuffer line =new StringBuffer();
		String tableName ="";
		String whereClause="";
		String OrderByClause ="";
		boolean replaceColumn = false;
		Vector<KeyNamePair> vSortNo = new Vector<KeyNamePair>();
		List <X_AD_ReportView_Col> reportViewCol = null;
		
		if (p_FileName == null)
			throw new AdempiereException(Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "FileName"));
		//else if (p_C_Bank_ID == 0)
		//	throw new AdempiereException(Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "C_Bank_ID"));
		else if (printFormat == null)
			throw new AdempiereException(Msg.translate(Env.getCtx(), "FillMandatory") + " " + Msg.translate(Env.getCtx(), "PrintFormat"));
		
		p_file=new File(p_FileName);
		
		if (printFormat.getAD_ReportView() != null && printFormat.getAD_ReportView_ID()>0){
			tableName = printFormat.getAD_ReportView().getAD_Table().getTableName();
			if (printFormat.getAD_ReportView().getWhereClause()!=null)
				whereClause = " AND "+printFormat.getAD_ReportView().getWhereClause();
			if ( printFormat.getAD_ReportView().getOrderByClause()!=null)
				OrderByClause = printFormat.getAD_ReportView().getOrderByClause();
			
			reportViewCol = new Query (this.getCtx(),X_AD_ReportView_Col.Table_Name,"AD_ReportView_ID = ?",null).setParameters(printFormat.getAD_ReportView().getAD_ReportView_ID()).list();
		}
		else{
			tableName =printFormat.getAD_Table().getTableName();
		}
		
		List<MPrintFormatItem> formatItem = new Query (getCtx(),MPrintFormatItem.Table_Name," AD_PrintFormat_ID = ? AND IsPrinted = ? ",null)
				.setParameters(printFormat.get_ID(),"Y")
				.setOrderBy(" SeqNo ")
				.list();
		
		sql.append("SELECT ");
		int items = 0;
		for (MPrintFormatItem formatI : formatItem){
			items++;
			
			if (reportViewCol != null){
				for (X_AD_ReportView_Col rwc : reportViewCol){
					MColumn colum = MColumn.get(getCtx(), rwc.getAD_Column_ID());
					if(formatI.getColumnName().equalsIgnoreCase(colum!=null?colum.getColumnName():"")){
						replaceColumn = true;
						sql.append(rwc.getFunctionColumn());
					}else{
						//
					}
				}
			}
			
			if (!replaceColumn){
				head.append(formatI.getColumnName());
				sql.append(formatI.getColumnName());
			}else{
				replaceColumn = false; //Inicializo.
			}
				
			if (formatItem.size()!=items){
				head.append(p_FormatType);
				sql.append(", ");
				vSortNo.add(new KeyNamePair(formatI.getSortNo(), formatI.getColumnName()));
				
			}
		}
		
		sql.append(" FROM "+tableName);
		//sql.append(" WHERE HR_Process_ID =");
		//sql.append(p_Record_ID);
		//sql.append(" AND C_Bank_ID = ");
		//sql.append(p_C_Bank_ID);
		sql.append(" WHERE AD_Client_ID = "+Env.getAD_Client_ID(Env.getCtx())+" ");
		sql.append(" "+whereClause);
		if (OrderByClause.length()>0){
			sql.append(" ORDER BY "+OrderByClause);
		}
			
		log.log(Level.INFO, "SQL: " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery(); 
			while (rs.next())
			{
				line = new StringBuffer();
				for (int i=1;i<=items;i++){
					line.append(rs.getString(i));
					if (i!=items)
						if (p_FormatType.contains("\\t"))
							line.append(p_FormatType.replace("\\t", "\t"));
						else
							line.append(p_FormatType);
				}
				line.append("\r\n");
				
				body.append(line.toString());
			}
			pstmt.close();
			
		}
		catch ( Exception e )
        {
			log.warning(Msg.translate(Env.getCtx(),e.getMessage()));
			throw new AdempiereException(e);
        }
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		try {
			java.io.FileWriter file = new java.io.FileWriter(p_file);
			java.io.BufferedWriter bw = new java.io.BufferedWriter(file);
			java.io.PrintWriter pw = new java.io.PrintWriter(bw); 
			head.append("\r\n");
			pw.write(head.toString());
			pw.write(body.toString());
			pw.close();
			bw.close();	
		}catch (IOException ioe) {
			log.warning(Msg.translate(Env.getCtx(),ioe.getMessage()));
			throw new AdempiereException(ioe);
		}
		m_created++;
		return p_file;
	}
	
	private boolean toAttach(File p_file) throws IOException{
		boolean succefull = false;
		if (p_file==null)
			return succefull;
		FileReader fr = new FileReader(p_file);
		BufferedReader buffer = new BufferedReader(fr);
		String linea = buffer.readLine();
		if (linea != null )
		{
			int  AD_Table_ID = MTable.getTable_ID("HR_Process");
			log.log(Level.INFO, "AD_Table_ID: " + AD_Table_ID + " - Record_ID: " + p_Record_ID);
			MAttachment attach =  MAttachment.get(getCtx(),AD_Table_ID,p_Record_ID);
			log.log(Level.INFO, "Contexto: " + getCtx().toString());
		
				log.log(Level.INFO, "AD_Table_ID: " + AD_Table_ID + " - Record_ID: " + p_Record_ID);
				attach = new  MAttachment(getCtx(),AD_Table_ID ,p_Record_ID,get_TrxName());
				attach.addEntry(p_file);
				attach.save();
				log.info("attach.save");		
				succefull = true;
		}
		else{
			succefull = false;
		}
		buffer.close();
		return succefull;
	}
}